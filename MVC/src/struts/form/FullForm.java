package struts.form;

import java.lang.reflect.Field;

import javax.servlet.http.HttpServletRequest;

public class FullForm {

	public FullForm(){
		
	}
	
	public static ActionForm Full(String filepath,HttpServletRequest request){
		
		ActionForm  action = null;
		try {
			Class clazz = Class.forName(filepath);
			action = (ActionForm)clazz.newInstance();
			Field[] field = clazz.getDeclaredFields();
			for(Field f:field){
				f.setAccessible(true);
				f.set(action,request.getParameter(f.getName()));
				f.setAccessible(false);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("Full方法异常！");
		}
		return action;
	}
}
