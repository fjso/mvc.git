package services.myService.panduan;

import java.util.Map;

import services.vo.MessageVO;

public interface PanduanService {

	   Map<String,MessageVO> getVO(String name);
}
